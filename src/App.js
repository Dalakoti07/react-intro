import React from 'react';
import './App.css';
import ShoppingList from './components/shoppingList'
import Game from './components/Game';
import 'bootstrap/dist/css/bootstrap.min.css';
import fakeComponent from './components/fakeComponent';


function App() {
  return (
    <div>
    <fakeComponent/>
    <ShoppingList message="This is Tic Tac Toe Presentation"/>
    <Game/>
    <ShoppingList message="End of page "/>
    </div>
      
    );
}

export default App;
