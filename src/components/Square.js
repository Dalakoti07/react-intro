import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';

function Square(props) {
    return (
      <Button variant="outline-primary" className="m-1 square" size="sm" onClick={props.onClick}>
        {props.value}
      </Button>
    );
  }

export default Square;